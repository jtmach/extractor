﻿namespace ExtractorTests
{
  using System;
  using Microsoft.VisualStudio.TestTools.UnitTesting;

  using Extractor;
  using System.Diagnostics;

  [TestClass]
  public class ExtractStepExtractTest
  {
    [TestMethod]
    public void ExtractStepExtractCleanData()
    {
      string initialData = "Test \r\n carriage \r return \n replacement";
      string replacedData = initialData;
      replacedData = replacedData.Replace("\r", " ");
      replacedData = replacedData.Replace("\n", " ");

      Assert.IsFalse(replacedData.Contains("\n") && replacedData.Contains("\r"));
    }
  }
}
