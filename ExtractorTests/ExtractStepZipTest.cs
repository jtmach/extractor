﻿namespace ExtractorTests
{
  using System;
  using Microsoft.VisualStudio.TestTools.UnitTesting;

  using Extractor;
  using System.Diagnostics;

  [TestClass]
  public class ExtractStepZipTest
  {
    [TestMethod]
    public void ExtractStepZipExecute()
    {
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.Start();
      
      ExtractStepZip extractStepZip = new Extractor.ExtractStepZip(false, "C:\\Temp\\ZipTest.txt", "C:\\Temp\\ZipTest.zip");
      extractStepZip.StatusUpdate += this.ExtractStepZip_StatusUpdate;
      Assert.IsTrue(extractStepZip.Execute());

      stopwatch.Stop();
      Debug.WriteLine("Total time: " + stopwatch.Elapsed.TotalSeconds + " seconds");
    }

    private void ExtractStepZip_StatusUpdate(object sender, EventArgs e)
    {
      ExtractEventArgs extractEventArgs = e as ExtractEventArgs;

      if (extractEventArgs != null)
      {
        Debug.WriteLine(extractEventArgs.Message);
      }
    }
  }
}
