﻿namespace ExtractorTests
{
  using System;
  using System.Diagnostics;
  using Extractor;
  using Microsoft.VisualStudio.TestTools.UnitTesting;

  [TestClass]
  public class ExtractStepEmailTest
  {
    [TestMethod]
    public void ExtractStepEmailExecute()
    {
      Stopwatch stopwatch = new Stopwatch();
      stopwatch.Start();

      ExtractStepEmail extractStepEmail = new Extractor.ExtractStepEmail(@"U:\CLMSeedlist\FY2014\CLMSeedlist-FY14-1_Test.pst", @"\\CLMSeedlist\Inbox");
      extractStepEmail.StatusUpdate += this.ExtractStepEmail_StatusUpdate;
      Assert.IsTrue(extractStepEmail.Execute());

      stopwatch.Stop();
      Debug.WriteLine("Total time: " + stopwatch.Elapsed.TotalSeconds + " seconds");
    }

    private void ExtractStepEmail_StatusUpdate(object sender, EventArgs e)
    {
      ExtractEventArgs extractEventArgs = e as ExtractEventArgs;

      if (extractEventArgs != null)
      {
        Debug.WriteLine(extractEventArgs.Message);
      }
    }
  }
}
