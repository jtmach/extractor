﻿namespace Extractor
{
  using ICSharpCode.SharpZipLib.Zip;
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.IO.Compression;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;

  public class ExtractStepZip : IExtractStep
  {
    public ExtractStepZip(XmlNode xmlNode)
    {
      this.DeleteSource = xmlNode.Attributes["DeleteSource"].Value == "True" ? true : false;
      this.ItemToCompress = xmlNode.Attributes["ItemToCompress"].Value;
      this.OutputFile = xmlNode.Attributes["OutputFile"].Value;
    }

    public ExtractStepZip(bool deleteSource, string itemToCompress, string outputFile)
    {
      this.DeleteSource = deleteSource;
      this.ItemToCompress = itemToCompress;
      this.OutputFile = outputFile;
    }

    public event EventHandler StatusUpdate;

    public bool DeleteSource { get; private set; }

    public string ItemToCompress { get; private set; }

    public string OutputFile { get; private set; }

    public bool Execute()
    {
      if (Directory.Exists(Parameters.ReplaceParameters(this.ItemToCompress)))
      {
        return this.CompressFolder();
      }
      else if (File.Exists(Parameters.ReplaceParameters(this.ItemToCompress)))
      {
        return this.CompressFile();
      }

      this.UpdateStatus("Item to compress not found");
      return false;
    }

    private bool CompressFile()
    {
      this.UpdateStatus("Compressing file");

      try
      {
        FileInfo fileInfo = new FileInfo(Parameters.ReplaceParameters(this.OutputFile));
        DirectoryInfo directoryInfo = fileInfo.Directory;

        if (!directoryInfo.Exists)
        {
          Directory.CreateDirectory(fileInfo.Directory.FullName);
        }

        using (ZipOutputStream stream = new ZipOutputStream(File.Create(Parameters.ReplaceParameters(this.OutputFile))))
        {
          stream.SetLevel(9); // 0 - store only to 9 - means best compression

          byte[] buffer = new byte[131072];

          // Using GetFileName makes the result compatible with XP
          // as the resulting path is not absolute.
          ZipEntry entry = new ZipEntry(Path.GetFileName(Parameters.ReplaceParameters(this.ItemToCompress)));
          entry.DateTime = DateTime.Now;
          stream.PutNextEntry(entry);

          using (FileStream fs = File.OpenRead(Parameters.ReplaceParameters(this.ItemToCompress)))
          {
            // Using a fixed size buffer here makes no noticeable difference for output
            // but keeps a lid on memory usage.
            double readBytes = 0.0;
            double lastPercentage = 0;
            int sourceBytes = 0;
            do
            {
              sourceBytes = fs.Read(buffer, 0, buffer.Length);
              readBytes = readBytes + sourceBytes;

              double percentage = (readBytes / fs.Length) * 100;
              if (Math.Truncate(percentage) != lastPercentage)
              {
                lastPercentage = Math.Truncate(percentage);
                this.UpdateStatus("Compressing file: " + Math.Truncate(percentage) + "% complete");
              }

              stream.Write(buffer, 0, sourceBytes);
              stream.Flush();
            } while (sourceBytes > 0);
          }

          stream.Finish();
          stream.Close();
        }

        if (this.DeleteSource)
        {
          File.Delete(Parameters.ReplaceParameters(this.ItemToCompress));
        }
      }
      catch (Exception ex)
      {
        this.UpdateStatus("Error: " + ex.Message);
        return false;
      }

      this.UpdateStatus("Finished Compressing");
      return true;
    }

    private bool CompressFolder()
    {
      this.UpdateStatus("Compressing folder");

      //FileInfo fileInfo = 

      //try
      //{
      //  // Depending on the directory this could be very large and would require more attention
      //  // in a commercial package.
      //  string[] filenames = Directory.GetFiles(args[0]);

      //  // 'using' statements guarantee the stream is closed properly which is a big source
      //  // of problems otherwise.  Its exception safe as well which is great.
      //  using (ZipOutputStream s = new ZipOutputStream(File.Create(args[1])))
      //  {

      //    s.SetLevel(9); // 0 - store only to 9 - means best compression

      //    byte[] buffer = new byte[4096];

      //    foreach (string file in filenames)
      //    {

      //      // Using GetFileName makes the result compatible with XP
      //      // as the resulting path is not absolute.
      //      ZipEntry entry = new ZipEntry(Path.GetFileName(file));

      //      // Setup the entry data as required.

      //      // Crc and size are handled by the library for seakable streams
      //      // so no need to do them here.

      //      // Could also use the last write time or similar for the file.
      //      entry.DateTime = DateTime.Now;
      //      s.PutNextEntry(entry);

      //      using (FileStream fs = File.OpenRead(file))
      //      {

      //        // Using a fixed size buffer here makes no noticeable difference for output
      //        // but keeps a lid on memory usage.
      //        int sourceBytes;
      //        do
      //        {
      //          sourceBytes = fs.Read(buffer, 0, buffer.Length);
      //          s.Write(buffer, 0, sourceBytes);
      //        } while (sourceBytes > 0);
      //      }
      //    }

      //    // Finish/Close arent needed strictly as the using statement does this automatically

      //    // Finish is important to ensure trailing information for a Zip file is appended.  Without this
      //    // the created file would be invalid.
      //    s.Finish();

      //    // Close is important to wrap things up and unlock the file.
      //    s.Close();
      //  }
      //}
      //catch (Exception ex)
      //{
      //  Console.WriteLine("Exception during processing {0}", ex);

      //  // No need to rethrow the exception as for our purposes its handled.
      //}

      throw new NotImplementedException();
      //return false;
    }

    private void UpdateStatus(string message)
    {
      if (this.StatusUpdate != null)
      {
        this.StatusUpdate(this, new ExtractEventArgs() { Message = message });
      }
    }
  }
}
