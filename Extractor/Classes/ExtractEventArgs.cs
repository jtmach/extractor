﻿namespace Extractor
{
  using System;

  public class ExtractEventArgs : EventArgs
  {
    public string Message { get; set; }
  }
}
