﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;

  public static class ConnectionsInfo
  {
    static ConnectionsInfo()
    {
      Connections = new List<ConnectionInfo>();
    }

    public static List<ConnectionInfo> Connections { get; set; }

    public static ConnectionInfo GetConnection(string connectionName)
    {
      foreach (ConnectionInfo connectionInfo in Connections)
      {
        if (connectionInfo.Name.ToLower() == connectionName.ToLower())
        {
          return connectionInfo;
        }
      }

      return null;
    }
  }
}
