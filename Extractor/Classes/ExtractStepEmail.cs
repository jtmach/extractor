﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.IO;
  using System.IO.Compression;
  using System.Linq;
  using System.Net;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;
  using Outlook = Microsoft.Office.Interop.Outlook;

  public class ExtractStepEmail : IExtractStep
  {
    private static Outlook.Application app = new Outlook.Application();
    private static Outlook.NameSpace nameSpace = app.GetNamespace("MAPI");

    public ExtractStepEmail(XmlNode xmlNode)
    {
      this.UpdateRegistry();
      this.PstFilePath = xmlNode.Attributes["PstFilePath"].Value;
      this.SourceFolder = xmlNode.Attributes["SourceFolder"].Value;
    }

    public ExtractStepEmail(string pstFilePath, string sourceFolder)
    {
      this.UpdateRegistry();
      this.PstFilePath = pstFilePath;
      this.SourceFolder = sourceFolder;
    }

    public event EventHandler StatusUpdate;

    public string PstFilePath { get; private set; }

    public string SourceFolder { get; private set; }
    
    public bool Execute()
    {
      string sourceFolderPath = Parameters.ReplaceParameters(this.SourceFolder);
      string pstPath = Parameters.ReplaceParameters(this.PstFilePath);

      try
      {
        Outlook.MAPIFolder sourceFolder = this.GetFolder(sourceFolderPath);
        if (sourceFolder == null)
        {
          this.UpdateStatus("Source folder (" + this.SourceFolder + ") not found.");
          return false;
        }

        Outlook.Store pstStore = this.GetPstStore(pstPath);
        Outlook.MAPIFolder destFolder = pstStore.GetRootFolder();

        List<int> destFolderItems = new List<int>();
        this.UpdateStatus("Creating list of items in destination folder");
        foreach (object item in destFolder.Items)
        {
          Outlook.MailItem mailItem = item as Outlook.MailItem;
          if (mailItem != null && !destFolderItems.Contains(mailItem.GetPropertyHashCode()))
          {
            destFolderItems.Add(mailItem.GetPropertyHashCode());
          }
        }

        this.UpdateStatus("Moving items to destination folder");
        int totalItems = sourceFolder.Items.Count;
        int percentageComplete = 0;
        int itemsMoved = 0;
        int itemsSkipped = 0;
        foreach (object item in sourceFolder.Items)
        {
          Outlook.MailItem mailItem = item as Outlook.MailItem;
          if (mailItem != null && !destFolderItems.Contains(mailItem.GetPropertyHashCode()))
          {
            mailItem.CopyToFolder(destFolder);
            itemsMoved++;
          }
          else
          {
            itemsSkipped++;
          }

          int tempPercentageComplete = Convert.ToInt32(((itemsMoved + itemsSkipped) / Convert.ToDouble(totalItems)) * 100);
          if (percentageComplete < tempPercentageComplete)
          {
            percentageComplete = tempPercentageComplete;
            if (percentageComplete % 2 == 0)
            {
              this.UpdateStatus(string.Format("{0}% Complete, {1} items moved, {2} items skipped.", percentageComplete, itemsMoved, itemsSkipped));
            }
          }
        }

        nameSpace.RemoveStore(destFolder);

        return true;
      }
      catch (Exception exception)
      {
        this.UpdateStatus("Error: " + exception.Message);
        return false;
      }
    }

    private Outlook.MAPIFolder GetFolder(string folderPath)
    {
      Outlook.Store foundStore = null;
      Outlook.MAPIFolder currentFolder = null;

      string backslash = @"\";
      try
      {
        if (folderPath.StartsWith(@"\\"))
        {
          folderPath = folderPath.Remove(0, 2);
        }

        string[] folders = folderPath.Split(backslash.ToCharArray());
        foreach (Outlook.Store store in nameSpace.Stores)
        {
          if (store.DisplayName.ToLower() == folders[0].ToLower())
          {
            foundStore = store;
            break;
          }
        }

        if (foundStore != null)
        {
          currentFolder = foundStore.GetRootFolder();
          Debug.Print(currentFolder.FolderPath);

          for (int i = 1; i < folders.Length; i++)
          {
            Outlook.MAPIFolder foundFolder = currentFolder.ContainsFolder(folders[i]);
            if (foundFolder != null)
            {
              currentFolder = currentFolder.Folders[folders[i]];
            }
            else
            {
              return null;
            }
          }
        }

        return currentFolder;
      }
      catch (Exception exception)
      {
        Console.WriteLine(exception.Message);
        return null;
      }
    }

    private Outlook.Store GetPstStore(string path)
    {
      nameSpace.AddStoreEx(path, Outlook.OlStoreType.olStoreUnicode);
      foreach (Outlook.Store store in nameSpace.Session.Stores)
      {
        if (store.FilePath == this.PstFilePath)
        {
          return store;
        }
      }

      return null;
    }

    private void UpdateRegistry()
    {
      WriteRegistryKey(@"Software\Policies\Microsoft\Office\12.0\Outlook", "DisablePST", "0");
      WriteRegistryKey(@"Software\Policies\Microsoft\Office\12.0\Outlook\pst", "pstdisablegrow", "0");
      WriteRegistryKey(@"Software\Policies\Microsoft\Office\14.0\Outlook", "DisablePST", "0");
      WriteRegistryKey(@"Software\Policies\Microsoft\Office\14.0\Outlook\pst", "pstdisablegrow", "0");
      WriteRegistryKey(@"Software\Policies\Microsoft\Office\15.0\Outlook", "DisablePST", "0");
      WriteRegistryKey(@"Software\Policies\Microsoft\Office\15.0\Outlook\pst", "pstdisablegrow", "0");
    }

    private void UpdateStatus(string message)
    {
      if (this.StatusUpdate != null)
      {
        this.StatusUpdate(this, new ExtractEventArgs() { Message = message });
      }
    }

    private static void WriteRegistryKey(string keyPath, string keyName, string value)
    {
      Microsoft.Win32.RegistryKey key;
      key = Microsoft.Win32.Registry.CurrentUser.CreateSubKey(keyPath);
      key.SetValue(keyName, value);
      key.Flush();
      key.Close();
    }
  }

  public static class Extensions
  {
    private static bool isCopyAllowed = true;

    public static Outlook.MAPIFolder ContainsFolder(this Outlook.MAPIFolder mapiFolder, string folderName)
    {
      foreach (Outlook.MAPIFolder folder in mapiFolder.Folders)
      {
        if (folder.Name.ToLower() == folderName.ToLower())
        {
          return folder;
        }
      }

      return null;
    }

    public static bool CopyToFolder(this Outlook.MailItem mailItem, Outlook.MAPIFolder destFolder)
    {
      try
      {
        Outlook.MailItem copyOfMailItem = mailItem;
        if (isCopyAllowed)
        {
          try
          {
            copyOfMailItem = mailItem.Copy() as Outlook.MailItem;
          }
          catch (UnauthorizedAccessException exception)
          {
            Debug.Print(exception.Message);
            isCopyAllowed = false;
          }
          copyOfMailItem.Move(destFolder);
        }
        else
        {
          copyOfMailItem.Move(destFolder);
        }
      }
      catch (UnauthorizedAccessException exception)
      {
        if (exception.Message != "The items were copied instead of moved because the original items cannot be deleted. You do not have sufficient permission to perform this operation on this object.  See the folder contact or your system administrator.")
        {
          throw;
        }
      }

      return true;
    }

    public static int GetPropertyHashCode(this Outlook.MailItem mailItem)
    {
      string uniqueIdentifier = mailItem.To + "~" + mailItem.Subject + "~" + mailItem.Body + "~" + mailItem.ReceivedTime;
      return uniqueIdentifier.GetHashCode();
    }
  }
}
