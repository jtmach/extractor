﻿namespace Extractor
{
  using System;
  using System.Data;
  using System.Data.OleDb;
  using System.IO;
  using System.Xml;

  public class ExtractStepExtract : IExtractStep
  {
    public ExtractStepExtract(XmlNode xmlNode)
    {
      this.ConnectionName = xmlNode.Attributes["ConnectionName"].Value;
      this.Delimiter = xmlNode.Attributes["Delimiter"].Value;
      this.ExportFile = xmlNode.Attributes["ExportFile"].Value;
      this.SqlText = Utility.LoadText(xmlNode.Attributes["SqlText"].Value);

      long lastExtractRecordCount = 0;

      if (xmlNode.Attributes["LastExtractRecordCount"] != null)
      {
        try
        {
          bool success = long.TryParse(xmlNode.Attributes["LastExtractRecordCount"].Value, out lastExtractRecordCount);
          this.LastExtractRecordCount = lastExtractRecordCount;

          xmlNode.Attributes["LastExtractRecordCount"].Value = "20";
        }
        catch (Exception ex)
        {
          string a = ex.Message;
        }
      }
    }

    public ExtractStepExtract(string connectionName, string delimiter, string exportFIle, string sqlText)
    {
      this.ConnectionName = connectionName;
      this.Delimiter = delimiter;
      this.ExportFile = exportFIle;
      this.SqlText = sqlText;
    }

    public event EventHandler StatusUpdate;

    public string ConnectionName { get; private set; }

    public string Delimiter { get; private set; }

    public string ExportFile { get; private set; }

    public long LastExtractRecordCount { get; private set; }

    public string SqlText { get; private set; }

    public bool Execute()
    {
      this.UpdateStatus("Starting extract");
      int recordCount = 0;
      string outputFilename = Parameters.ReplaceParameters(this.ExportFile);
      if (File.Exists(outputFilename))
      {
        File.Delete(outputFilename);
      }

      ConnectionInfo connectionInfo = ConnectionsInfo.GetConnection(this.ConnectionName);
      if (connectionInfo == null)
      {
        throw new Exception("Connection (" + this.ConnectionName + ") is not defined");
      }

      FileInfo fileInfo = new FileInfo(outputFilename);
      DirectoryInfo directoryInfo = fileInfo.Directory;

      if (!directoryInfo.Exists)
      {
        Directory.CreateDirectory(fileInfo.Directory.FullName);
      }

      StreamWriter streamWriter = new StreamWriter(outputFilename);
      OleDbConnection connection = new OleDbConnection(connectionInfo.ConnectionString);
      OleDbCommand command = new OleDbCommand(Parameters.ReplaceParameters(this.SqlText));
      command.Connection = connection;
      command.CommandTimeout = 0;

      try
      {
        connection.Open();
        OleDbDataReader dataReader = command.ExecuteReader();

        for (int i = 0; i < dataReader.FieldCount; i++)
        {
          streamWriter.Write((i == 0 ? string.Empty : this.Delimiter) + dataReader.GetName(i));
        }

        streamWriter.WriteLine();

        while (dataReader.Read())
        {
          for (int i = 0; i < dataReader.FieldCount; i++)
          {
            streamWriter.Write((i == 0 ? string.Empty : this.Delimiter) + this.CleanData(dataReader[i]));
          }

          streamWriter.WriteLine();

          recordCount++;
          if (recordCount % 10000 == 0)
          {
            this.UpdateStatus("Extracted " + string.Format("{0:n0}", recordCount) + " records");
          }
        }

        dataReader.Close();
        streamWriter.Flush();
        streamWriter.Close();

        this.UpdateStatus("Extracted " + string.Format("{0:n0}", recordCount) + " records");
      }
      catch(Exception exception)
      {
        this.UpdateStatus(exception.Message);
        return false;
      }
      finally
      {
        if (connection.State == ConnectionState.Open)
        {
          connection.Close();
        }
      }

      return true;
    }

    private string CleanData(object startingString)
    {
      string cleanedString = startingString.ToString();

      cleanedString = cleanedString.Replace("\r", " ");
      cleanedString = cleanedString.Replace("\n", " ");

      return cleanedString;
    }

    private void UpdateStatus(string message)
    {
      if (this.StatusUpdate != null)
      {
        this.StatusUpdate(this, new ExtractEventArgs() { Message = message });
      }
    }
  }
}
