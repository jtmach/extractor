﻿namespace Extractor
{
  using System.IO;

  public static class Utility
  {
    public static string LoadText(string originalString)
    {
      if (originalString.StartsWith("*") && originalString.EndsWith("*"))
      {
        originalString = originalString.Substring(1, originalString.Length-2);
        if (File.Exists(originalString))
        {
          StreamReader streamReader = new StreamReader(originalString);
          originalString = streamReader.ReadToEnd();
          streamReader.Close();
        }
      }

      return originalString;
    }
  }
}
