﻿namespace Extractor
{
  using System;
  using System.Data;
  using System.Data.OleDb;
  using System.IO;
  using System.Xml;

  public class ExtractStepAction : IExtractStep
  {
    public ExtractStepAction(XmlNode xmlNode)
    {
      this.ConnectionName = xmlNode.Attributes["ConnectionName"].Value;
      this.SqlText = xmlNode.Attributes["SqlText"].Value;
    }

    public ExtractStepAction(string connectionName, string sqlText)
    {
      this.ConnectionName = connectionName;
      this.SqlText = sqlText;
    }

    public event EventHandler StatusUpdate;

    public string ConnectionName { get; private set; }

    public string SqlText { get; private set; }

    public bool Execute()
    {
      this.UpdateStatus("Starting Action");

      ConnectionInfo connectionInfo = ConnectionsInfo.GetConnection(this.ConnectionName);
      if (connectionInfo == null)
      {
        throw new Exception("Connection (" + this.ConnectionName + ") is not defined");
      }

      OleDbConnection connection = new OleDbConnection(connectionInfo.ConnectionString);
      OleDbCommand command = new OleDbCommand(Parameters.ReplaceParameters(this.SqlText));
      command.Connection = connection;
      command.CommandTimeout = 0;

      try
      {
        connection.Open();
        command.ExecuteNonQuery();
        this.UpdateStatus("Action completed successfully");
      }
      catch (Exception exception)
      {
        this.UpdateStatus(exception.Message);
        return false;
      }
      finally
      {
        if (connection.State == ConnectionState.Open)
        {
          connection.Close();
        }
      }

      return true;
    }

    private void UpdateStatus(string message)
    {
      if (this.StatusUpdate != null)
      {
        this.StatusUpdate(this, new ExtractEventArgs() { Message = message });
      }
    }
  }
}
