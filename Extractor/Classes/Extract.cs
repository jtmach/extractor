﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;

  public class Extract
  {
    public Extract(XmlNode xmlNode)
    {
      this.ExtractSteps = new List<IExtractStep>();
      this.Frequency = xmlNode.Attributes["Frequency"].Value;
      this.Name = xmlNode.Attributes["Name"].Value;

      DateTime lastExported = DateTime.MinValue;
      DateTime.TryParse(xmlNode.Attributes["LastExported"].Value, out lastExported);
      this.LastExported = lastExported;

      foreach (XmlNode child in xmlNode.ChildNodes)
      {
        switch (child.Name.ToUpper())
        {
          case "ACTION":
            this.ExtractSteps.Add(new ExtractStepAction(child));
            break;
          case "EMAIL":
            this.ExtractSteps.Add(new ExtractStepEmail(child));
            break;
          case "FTP":
            this.ExtractSteps.Add(new ExtractStepFtp(child));
            break;
          case "EXTRACT":
            this.ExtractSteps.Add(new ExtractStepExtract(child));
            break;
          case "ZIP":
            this.ExtractSteps.Add(new ExtractStepZip(child));
            break;
        }
      }
    }

    public string Frequency { get; set; }

    public string Name { get; set; }

    public DateTime LastExported { get; set; }

    public long LastExportedRecordCount { get; set; }

    public List<IExtractStep> ExtractSteps { get; set; }
  }
}
