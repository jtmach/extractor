﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;

  public class ConnectionInfo
  {
    public ConnectionInfo(XmlNode xmlNode)
    {
      this.ConnectionString = xmlNode.Attributes["ConnectionString"].Value;
      this.Name = xmlNode.Attributes["Name"].Value;
    }

    public string ConnectionString { get; private set; }

    public string Name { get; private set; }
  }
}
