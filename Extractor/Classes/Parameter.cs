﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;

  public class Parameter
  {
    public Parameter(string name, string value)
    {
      this.Name = name;
      this.Value = value;
    }

    public string Name { get; private set; }

    public string Value { get; set; }

    public override string ToString()
    {
      return this.Name + " - " + this.Value;
    }
  }
}
