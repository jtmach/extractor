﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.IO;
  using System.IO.Compression;
  using System.Linq;
  using System.Net;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;

  public class ExtractStepFtp : IExtractStep
  {
    public ExtractStepFtp(XmlNode xmlNode)
    {
      this.Directory = xmlNode.Attributes["Directory"].Value;
      this.DestFileName = xmlNode.Attributes["DestFileName"].Value;
      this.Password = xmlNode.Attributes["Password"].Value;
      this.ServerName = xmlNode.Attributes["ServerName"].Value;
      this.SourceFileName = xmlNode.Attributes["SourceFileName"].Value;
      this.UserName = xmlNode.Attributes["UserName"].Value;
    }

    public ExtractStepFtp(string directory, string destFileName, string password, string serverName, string sourceFileName, string userName)
    {
      this.Directory = directory;
      this.DestFileName = destFileName;
      this.Password = password;
      this.ServerName = serverName;
      this.SourceFileName = sourceFileName;
      this.UserName = userName;
    }

    public event EventHandler StatusUpdate;

    public string Directory { get; private set; }

    public string DestFileName { get; private set; }
    
    public string Password { get; private set; }
    
    public string ServerName { get; private set; }

    public string SourceFileName { get; private set; }

    public string UserName { get; private set; }

    public bool Execute()
    {
      // Get the object used to communicate with the server.
      FtpWebRequest request = (FtpWebRequest)WebRequest.Create("ftp://" + this.ServerName + "/" + Parameters.ReplaceParameters(this.Directory) + "/" + Parameters.ReplaceParameters(this.DestFileName));
      request.Credentials = new NetworkCredential(this.UserName, this.Password);
      request.Method = WebRequestMethods.Ftp.UploadFile;
      request.Proxy = null;
      request.UsePassive = true;

      // Copy the contents of the file to the request stream.
      StreamReader sourceStream = new StreamReader(Parameters.ReplaceParameters(this.SourceFileName));
      byte[] fileContents = Encoding.UTF8.GetBytes(sourceStream.ReadToEnd());
      sourceStream.Close();
      request.ContentLength = fileContents.Length;

      Stream requestStream = request.GetRequestStream();
      requestStream.Write(fileContents, 0, fileContents.Length);
      requestStream.Close();

      FtpWebResponse response = (FtpWebResponse)request.GetResponse();

      this.UpdateStatus("Upload File Complete, status " + response.StatusDescription);

      response.Close();

      return response.StatusDescription.StartsWith("226");
    }

    private void UpdateStatus(string message)
    {
      if (this.StatusUpdate != null)
      {
        this.StatusUpdate(this, new ExtractEventArgs() { Message = message });
      }
    }
  }
}
