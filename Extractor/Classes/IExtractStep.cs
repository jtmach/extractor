﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;

  public interface IExtractStep
  {
    bool Execute();

    event EventHandler StatusUpdate;
  }
}
