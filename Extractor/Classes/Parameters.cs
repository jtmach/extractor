﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Data;
  using System.Data.OleDb;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Xml;

  public static class Parameters
  {
    private static List<Parameter> parameters = new List<Parameter>();

    public static void Add(XmlNode xmlNode)
    {
      if (xmlNode.Name == "SqlParameter")
      {
        LoadSqlParameter(xmlNode);
      }
      else if (xmlNode.Name == "StaticParameter")
      {
        parameters.Add(new Parameter(xmlNode.Attributes["Name"].Value, xmlNode.Attributes["Value"].Value));
      }
    }

    public static List<Parameter> List
    {
      get
      {
        return parameters;
      }
    }

    public static void SetValue(int index, string newValue)
    {
      if (index < 0 || index > parameters.Count - 1)
      {
        return;
      }

      parameters[index].Value = newValue;
    }

    public static string ReplaceParameters(string parameterizedString)
    {
      string finalString = parameterizedString;

      // We will support up to 10 levels of parameter depth
      for (int depth = 0; depth < 10; depth++)
      {
        foreach (Parameter parameter in parameters)
        {
          finalString = finalString.Replace(parameter.Name, parameter.Value);
        }
      }

      // Replacing xml escape characters
      finalString = finalString.Replace("&lt;", "<");
      finalString = finalString.Replace("&gt;", ">");
      finalString = finalString.Replace("&quot;", "\"");
      finalString = finalString.Replace("&amp;", "&");
      finalString = finalString.Replace("&apos;", "'");

      return finalString;
    }

    private static void LoadSqlParameter(XmlNode xmlNode)
    {
      ConnectionInfo connectionInfo = ConnectionsInfo.GetConnection(xmlNode.Attributes["ConnectionName"].Value);
      if (connectionInfo == null)
      {
        throw new Exception("Connection (" + xmlNode.Attributes["ConnectionName"].Value + ") is not defined");
      }

      OleDbConnection connection = new OleDbConnection(connectionInfo.ConnectionString);
      OleDbCommand command = new OleDbCommand(Utility.LoadText(xmlNode.Attributes["SqlText"].Value));
      command.Connection = connection;
      command.CommandTimeout = 0;

      try
      {
        connection.Open();
        OleDbDataReader dataReader = command.ExecuteReader();

        if (dataReader.HasRows)
        {
          dataReader.Read();
        }

        for (int i = 0; i < dataReader.FieldCount; i++)
        {
          parameters.Add(new Parameter("[" + dataReader.GetName(i) + "]", dataReader[i].ToString()));
        }

        dataReader.Close();
      }
      catch (Exception)
      {
        throw;
      }
      finally
      {
        if (connection.State == ConnectionState.Open)
        {
          connection.Close();
        }
      }
    }
  }
}
