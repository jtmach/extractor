﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics;
  using System.IO;
  using System.Threading;
  using System.Threading.Tasks;
  using System.Windows;
  using System.Windows.Forms;
  using System.Xml;

  public partial class MainWindow : Window
  {
    private XmlDocument extractDefinitions;

    public MainWindow()
    {
      InitializeComponent();
      CheckParametersFile();
      ProgramArguments.ProcessArguments(Environment.GetCommandLineArgs());
      LoadMetadata();
    }

    private void CheckParametersFile()
    {
      if (string.IsNullOrEmpty(Properties.Settings.Default.ExtractsDirectory))
      {
        if (!Directory.Exists(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) ,"Extractor")))
        {
          Directory.CreateDirectory(System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Extractor"));
        }

        FolderBrowserDialog folderBrowser = new FolderBrowserDialog();
        folderBrowser.SelectedPath = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Extractor");
        if (folderBrowser.ShowDialog() == System.Windows.Forms.DialogResult.OK)
        {
          Properties.Settings.Default.ExtractsDirectory = folderBrowser.SelectedPath;
          Properties.Settings.Default.Save();

          if (!File.Exists(Properties.Settings.Default.ExtractsDirectory + "\\Extracts.xml"))
          {
            XmlDocument xmlDoc = new XmlDocument();
            XmlDeclaration declaration = xmlDoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = xmlDoc.DocumentElement;
            xmlDoc.InsertBefore(declaration, root);

            XmlElement dataExtracts = xmlDoc.CreateElement(string.Empty, "DataExtracts", string.Empty);
            xmlDoc.AppendChild(dataExtracts);

            XmlElement Connections = xmlDoc.CreateElement(string.Empty, "Connections", string.Empty);
            dataExtracts.AppendChild(Connections);

            XmlElement Extracts = xmlDoc.CreateElement(string.Empty, "Extracts", string.Empty);
            dataExtracts.AppendChild(Extracts);

            XmlElement Parameters = xmlDoc.CreateElement(string.Empty, "Parameters", string.Empty);
            dataExtracts.AppendChild(Parameters);

            xmlDoc.Save(Properties.Settings.Default.ExtractsDirectory + "\\Extracts.xml");

            Process.Start(Properties.Settings.Default.ExtractsDirectory + "\\Extracts.xml");
          }
        }
      }

      if (File.Exists(Properties.Settings.Default.ExtractsDirectory + "\\Extracts.xml"))
      {
        extractDefinitions = new XmlDocument();
        extractDefinitions.Load(Properties.Settings.Default.ExtractsDirectory + "\\Extracts.xml");
      }
      else
      {
        System.Windows.MessageBox.Show("Extracts.xml file not found.");
        Environment.Exit(1);
      }
    }

    private void LoadConnections(XmlNode xmlNode)
    {
      foreach (XmlNode connectionNode in xmlNode.ChildNodes)
      {
        ConnectionsInfo.Connections.Add(new ConnectionInfo(connectionNode));
      }
    }

    private void LoadExtracts(XmlNode xmlNode)
    {
      foreach (XmlNode extractNode in xmlNode.ChildNodes)
      {
        Extract extract = new Extract(extractNode);
        DefinedExtract defExtract = new DefinedExtract(extract);

        if (ProgramArguments.AutoRun)
        {
          if (ProgramArguments.AutoRunMonthly && extract.Frequency.ToLower() == "monthly")
          {
            defExtract.IsSelected = true;
          }
          else if (ProgramArguments.AutoRunNames.Contains(extract.Name.ToLower()))
          {
            defExtract.IsSelected = true;
          }
          else if (ProgramArguments.AutoRunWeekly && extract.Frequency.ToLower() == "weekly")
          {
            defExtract.IsSelected = true;
          }
          else
          {
            defExtract.IsSelected = false;
          }
        }
        else
        {
          defExtract.IsSelected = true;
        }


        this.DefinedExtracts.Children.Add(defExtract);
      }
    }

    private void LoadMetadata()
    {
      //extractDefinitions = new XmlDocument();
      //extractDefinitions.Load(Environment.CurrentDirectory + "\\Extracts.xml");

      foreach (XmlNode rootNodes in extractDefinitions.ChildNodes)
      {
        foreach (XmlNode node in rootNodes.ChildNodes)
        {
          switch (node.Name)
          {
            case "Connections":
              this.LoadConnections(node);
              break;
            case "Extracts":
              this.LoadExtracts(node);
              break;
            case "Parameters":
              this.LoadParameters(node);
              break;
            case "Users":
              break;
          }
        }
      }
    }

    private void LoadParameters(XmlNode xmlNode)
    {
      Task task = new Task(() =>
      {
        bool errorsLoadingParameters = false;
        int loadedParameters = 0;
        int staticParameterCount = 0;
        UpdateParametersStatus(loadedParameters, xmlNode.ChildNodes.Count, errorsLoadingParameters);
        foreach (XmlNode parameterNode in xmlNode.ChildNodes)
        {
          if (parameterNode.Name == "StaticParameter")
          {
            Parameters.Add(parameterNode);
            staticParameterCount++;
            loadedParameters++;
            UpdateParametersStatus(loadedParameters, xmlNode.ChildNodes.Count, errorsLoadingParameters);
          }
        }

        CancellationToken cancellationToken = new CancellationToken();
        Task<int>[] sqlParameterTasks = new Task<int>[xmlNode.ChildNodes.Count - staticParameterCount];
        int sqlParameterTaskIndex = 0;
        foreach (XmlNode parameterNode in xmlNode.ChildNodes)
        {
          if (parameterNode.Name == "SqlParameter")
          {
            Func<object, int> action = (object obj) =>
            {
              try
              {
                Parameters.Add(parameterNode);
                loadedParameters++;
                UpdateParametersStatus(loadedParameters, xmlNode.ChildNodes.Count, errorsLoadingParameters);
              }
              catch (Exception ex)
              {
                errorsLoadingParameters = true;
                UpdateParametersStatus(loadedParameters, xmlNode.ChildNodes.Count, errorsLoadingParameters);
                Debug.Write("Error loading parameter (" + sqlParameterTaskIndex + ")\n" + ex.Message);
              }

              return 0;
            };

            sqlParameterTasks[sqlParameterTaskIndex] = Task<int>.Factory.StartNew(action, cancellationToken);
            sqlParameterTaskIndex++;
          }
        }

        Task.WaitAll(sqlParameterTasks, cancellationToken);

        Dispatcher.Invoke(new Action(() =>
        {
          this.LoadingControl.Visibility = System.Windows.Visibility.Hidden;
          this.EditParameters.IsEnabled = true;
          this.Run.IsEnabled = true;

          foreach (Parameter parameter in Parameters.List)
          {
            EditParameter editParameter = new EditParameter(Parameters.List.IndexOf(parameter));
            editParameter.ParameterName.Content = parameter.Name;
            editParameter.ParameterValue.Text = parameter.Value;
            this.DefinedParameters.Children.Add(editParameter);
          }

          if (ProgramArguments.AutoRun)
          {
            this.RunSelectedExtracts();
          }
        }));
      });
      task.Start();
    }

    private void LoadUsers()
    { }

    private void UpdateParametersStatus(int current, int total, bool errorsOccured)
    {
      Dispatcher.Invoke(new Action(() =>
      {
        this.LoadingText.Text = string.Format("Loading Parameters {0} of {1}{2}", current.ToString(), total.ToString(), errorsOccured ? "\nErrors loading parameters" : "");
      }));
    }

    private void ClearFinished_Click(object sender, RoutedEventArgs e)
    {
      List<RunningExtract> extractsToRemove = new List<RunningExtract>();
      foreach (UIElement control in this.RunningExtracts.Children)
      {
        RunningExtract runningExtract = control as RunningExtract;
        if (runningExtract != null && runningExtract.IsComplete)
        {
          extractsToRemove.Add(runningExtract);
        }
      }

      foreach (RunningExtract runningExtract in extractsToRemove)
      {
        this.RunningExtracts.Children.Remove(runningExtract);
      }
    }

    private void EditParameters_Click(object sender, RoutedEventArgs e)
    {
      if (this.ParametersScrollViewer.Visibility == Visibility.Hidden)
      {
        this.ParametersScrollViewer.Visibility = Visibility.Visible;
        this.EditParameters.Content = "Hide Parameters";
      }
      else
      {
        this.ParametersScrollViewer.Visibility = Visibility.Hidden;
        this.EditParameters.Content = "Edit Parameters";
      }
    }

    private void Run_Click(object sender, RoutedEventArgs e)
    {
      this.RunSelectedExtracts();
    }

    private void SelectAll_Click(object sender, RoutedEventArgs e)
    {
      foreach (UIElement control in this.DefinedExtracts.Children)
      {
        DefinedExtract definedExtract = control as DefinedExtract;
        if (definedExtract != null)
        {
          definedExtract.IsSelected = true;
        }
      }
    }

    private void UnselectAll_Click(object sender, RoutedEventArgs e)
    {
      foreach (UIElement control in this.DefinedExtracts.Children)
      {
        DefinedExtract definedExtract = control as DefinedExtract;
        if (definedExtract != null)
        {
          definedExtract.IsSelected = false;
        }
      }
    }

    private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
    {
      //this.extractDefinitions.Save(Environment.CurrentDirectory + "\\Extracts.xml");
    }

    private void RunSelectedExtracts()
    {
      foreach (UIElement control in this.DefinedExtracts.Children)
      {
        DefinedExtract definedExtract = control as DefinedExtract;
        if (definedExtract != null && definedExtract.IsSelected)
        {
          bool alreadyRunning = false;

          foreach (UIElement runningControl in this.RunningExtracts.Children)
          {
            RunningExtract runningExtract = runningControl as RunningExtract;
            if (runningExtract != null && runningExtract.Extract.Name == definedExtract.Extract.Name)
            {
              alreadyRunning = true;
            }
          }

          if (!alreadyRunning)
          {
            RunningExtract runningExtract = new RunningExtract(definedExtract.Extract);
            this.RunningExtracts.Children.Add(runningExtract);
          }
        }
      }
    }
  
    private static class ProgramArguments
    {
      public static bool AutoRun { get; private set; }

      public static bool AutoRunMonthly { get; private set; }

      public static System.Collections.ArrayList AutoRunNames { get; private set; }

      public static bool AutoRunWeekly { get; private set; }

      public static bool CloseWhenFinished { get; private set; }

      public static void ProcessArguments(string[] arguments)
      {
        AutoRunNames = new System.Collections.ArrayList();

        foreach (string argument in arguments)
        {
          switch (argument.ToLower())
          {
            case "/auto":
              AutoRun = true;
              break;
            case "/close":
              CloseWhenFinished = true;
              break;
            case "monthly":
              if (AutoRun) { AutoRunMonthly = true; }
              break;
            case "weekly":
              if (AutoRun) { AutoRunWeekly = true; }
              break;
            default:
              if (AutoRun && !AutoRunNames.Contains(argument.ToLower()))
              {
                AutoRunNames.Add(argument.ToLower()); 
              }
              break;
          }
        }
      }
    }
  }
}
