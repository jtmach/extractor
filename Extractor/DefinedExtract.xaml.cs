﻿namespace Extractor
{
  using System.Windows;
  using System.Windows.Controls;

  public partial class DefinedExtract : UserControl
  {
    public DefinedExtract(Extract dataExtract)
    {
      InitializeComponent();
      this.Extract = dataExtract;
      this.Frequency.Text = "Frequency: " + dataExtract.Frequency;
      this.LastExtractDate.Text = "Last Extracted: " + dataExtract.LastExported;
      this.ExtractName.Text = dataExtract.Name;
    }

    public Extract Extract { get; private set; }

    public bool IsSelected
    {
      get
      {
        return this.RunExtract.IsChecked ?? false;
      }

      set
      {
        this.RunExtract.IsChecked = value;
      }
    }

    private void Expander_Collapsed(object sender, RoutedEventArgs e)
    {
      this.Details.Visibility = System.Windows.Visibility.Collapsed;
    }

    private void Expander_Expanded(object sender, RoutedEventArgs e)
    {
      this.Details.Visibility = System.Windows.Visibility.Visible;
    }
  }
}
