﻿namespace Extractor
{
  using System.Windows;
  using System.Windows.Controls;

  public partial class EditParameter : UserControl
  {
    public EditParameter(int parameterIndex)
    {
      InitializeComponent();
      this.ParameterIndex = parameterIndex;
    }

    public int ParameterIndex { get; set; }

    private void ParameterValue_LostFocus(object sender, RoutedEventArgs e)
    {
      Parameters.SetValue(ParameterIndex, this.ParameterValue.Text);
    }
  }
}
