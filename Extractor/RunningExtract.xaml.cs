﻿namespace Extractor
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Threading.Tasks;
  using System.Windows;
  using System.Windows.Controls;
  using System.Windows.Data;
  using System.Windows.Documents;
  using System.Windows.Input;
  using System.Windows.Media;
  using System.Windows.Media.Imaging;
  using System.Windows.Navigation;
  using System.Windows.Shapes;

  public partial class RunningExtract : UserControl
  {
    public RunningExtract(Extract dataExtract)
    {
      this.InitializeComponent();
      this.Extract = dataExtract;
      this.ExtractName.Text = dataExtract.Name;
      this.ExtractSteps.Text = "Step 1 of " + dataExtract.ExtractSteps.Count;
      this.IsComplete = false;

      this.StartExecution();
    }

    public Extract Extract { get; private set; }

    public bool IsComplete { get; private set; }

    public void MarkCompleted(bool success)
    {
      this.MainGrid.Background = new SolidColorBrush(success ? Colors.LightGreen : Colors.Red);

      if (success)
      {
        this.ExtractStatus.Text = string.Empty;
        this.ExtractSteps.Text = "Finished";
      }

      this.IsComplete = true;
    }

    public void StartExecution()
    {
      Task task = new Task(() =>
      {
        int extractStepIndex = 1;
        bool success = true;
        foreach (IExtractStep extractStep in this.Extract.ExtractSteps)
        {
          Dispatcher.Invoke(new Action(() =>
          {
            this.ExtractSteps.Text = "Step " + extractStepIndex + " of " + this.Extract.ExtractSteps.Count;
          }));

          extractStep.StatusUpdate += this.ExtractStep_StatusUpdate;
          success = extractStep.Execute();

          if (!success)
          {
            break; 
          }

          extractStepIndex++;
        }

        Dispatcher.Invoke(new Action(() =>
        {
          this.MarkCompleted(success);
        }));
      });
      task.Start();
    }

    private void ExtractStep_StatusUpdate(object sender, EventArgs e)
    {
      this.Dispatcher.Invoke(new Action(() =>
      {
        ExtractEventArgs extractEventArgs = e as ExtractEventArgs;

        if (extractEventArgs != null)
        {
          this.ExtractStatus.Text = extractEventArgs.Message;
        }
      }));
    }
  }
}
